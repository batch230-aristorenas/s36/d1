const express = require("express");

// express Router() method allows access to HTTP methods
const router = express.Router();

const taskController = require ("../controllers/taskController");


// Create - task routes
router.post("/addTask", taskController.createTaskController);

//get ALL task
router.get("/allTask", taskController.getAllTaskController);

// Delete
router.delete("/deleteTask/:taskId", taskController.deleteTaskController);

//PUT
router.put("/:id/archive", (req, res) => {
	taskController.changeNameToArchive(req.params.id).then(resultFromController => res.send(resultFromController));
}) 

// PATCH
router.patch("/updateTask/:taskId", taskController.updateTaskNameController);

// Discussion
router.get("/getName/:taskId", taskController.getSpecificTaskName);


//ACTIVITY
router.get("/:taskId", taskController.getOneTaskController);

router.put("/:taskId/complete", taskController.updateTaskController);



module.exports = router;